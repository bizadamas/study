﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApIEmpty.Models;

namespace WebApIEmpty.Controllers
{
    //[HttpBasicAuthorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductsController : ApiController
    {
        Product[] products = new Product[] 
        { 
            new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 }, 
            new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M }, 
            new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M } 
        };



       // [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IEnumerable<Product> GetAll()
        {

            return products;
        }



        public IHttpActionResult Get(int id)
        {
            var product = products.FirstOrDefault((p) => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
        public IHttpActionResult Get(string name)
        {
            var product = products.FirstOrDefault((p) => p.Name==name);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
    }
}
