﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getCpuTest
{
    public class BinarySearchTree
    {
        public BstNode root;

        public BinarySearchTree()
        {
            root = null;
        }

        public BinarySearchTree(BstNode node)
        {
            root = node;
        }

        /// <summary>
        /// 插入节点
        /// </summary>
        /// <param name="i"></param>
        public void Insert(int i)
        {
            BstNode newNode = new BstNode(i);
            if (root == null)
            {
                root = newNode;

            }
            else
            {
                BstNode current = root;
                BstNode parent;
                while (true)
                {
                    parent = current;
                    if (i < current.Data)
                    {
                        current = current.Left;
                        if (current == null)
                        {
                            parent.Left = newNode;
                            break;
                        }
                    }
                    else
                    {
                        current = current.Right;
                        if (current == null)
                        {
                            parent.Right = newNode;
                            break;
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 中序遍历
        /// </summary>
        /// <param name="root"></param>
        public void InOrder(BstNode root)
        {
            if (root != null)
            {
                InOrder(root.Left);
                root.DisplayNode();
                InOrder(root.Right);
            }
        }

        //先序
        public void PreOrder(BstNode root)
        {
            if (root != null)
            {
                root.DisplayNode();
                PreOrder(root.Left);
                PreOrder(root.Right);
            }
        }

        /// <summary>
        /// 后续遍历
        /// </summary>
        /// <param name="root"></param>
        public void ProstOrder(BstNode root)
        {
            if (root != null)
            {
                
                PreOrder(root.Left);
                PreOrder(root.Right);
                root.DisplayNode();
               
            }
        }


        public int GetMax()
        {
            if (root == null)
                return -1;

            BstNode b = root;
            while (b.Right != null)
            {

                b = b.Right;

            }

            return b.Data;
        }


        public int GetMin()
        {
            if (root == null)
                return -1;

            BstNode b = root;
            while (b.Left != null)
            {

                b = b.Left;

            }

            return b.Data;
        }



    }
}
