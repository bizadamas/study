﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getCpuTest
{
    public class TestArray
    {
        private int[] arr;
        private int upper;
        private int numElements;

        public TestArray(int size)
        {
            arr = new int[size];
            upper = size - 1;
            numElements = 0;
        }

        public void Insert(int item)
        {
            arr[numElements] = item;
            numElements++;
        }

        public void DisplayElements()
        {
            for (int i = 0; i <= upper; i++)
            {
                Console.Write(arr[i] + " ");
            }
        }

        public void Clear()
        {
            for (int i = 0; i <= upper; i++)
            {
                arr[i] = 0;
            }
            numElements = 0;
        }

        /// <summary>
        /// 冒泡排序
        /// </summary>
        public void BubbleSort()
        {
            for (int i = 0; i < upper; i++)
            {
                for (int j = 0; j <upper- i; j++)
                {
                    int temp;
                    if (arr[j] > arr[j + 1])
                    {
                        temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
        }
        /// <summary>
        /// 选择排序
        /// </summary>
        public void SelectionSort()
        {
            int min,temp;
            for (int i = 0; i <upper; i++)
            {
                min=i;
                for (int j = i + 1; j <= upper; j++)
                {
                    if (arr[j] < arr[min])
                    {
                        min = j;
                    }
                }

                temp = arr[i];
                arr[i] = arr[min];
                arr[min] = temp;
            }
        }
        /// <summary>
        /// 插入排序
        /// </summary>
        public void InsertSort()
        {
            int j, temp;
            for (int i = 1; i <= upper; i++)
            {
                temp = arr[i];
                j = i;
                while (j > 0 && arr[j - 1] >= temp)
                {
                    arr[j] = arr[j-1];
                    j -= 1;
                }
                arr[j] = temp;
            }
        }


    }
}
